﻿// homework_18.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

template<typename T>
class MyStack
{
    T value;
    MyStack<T>* next;
    int count;
    MyStack(const T _value, MyStack<T>* _next) : value{ _value }, next{ _next }, count{ 0 }
    {}

public:
    MyStack() : value{ 0 }, next{ this }, count{ 0 }
    {}

    void push(const T value)
    {
        this->value = value;
        this->next = new MyStack<T>(value, next);
        count++;
    }

    void GetCount() {
        return count;
    }

    T pop()
    {
        if (next != this) {
            T _value = next->value;
            MyStack<T>* _stack = next->next;
            delete next;
            count--;
            next = _stack;
            
            return _value;
        }
    }
};

int main()
{
    MyStack<int> stack2;
    stack2.push(1);
    stack2.push(2);
    std::cout << stack2.pop() << "\n";
    std::cout << stack2.pop() << "\n\n";

    MyStack<int*> stack;
    
    stack.push(new int(1));
    stack.push(new int(2));
    stack.push(new int(3));
    stack.push(new int(4));
    std::cout << *stack.pop() << "\n";
    std::cout << *stack.pop() << "\n";
    stack.push(new int(5));
    std::cout << *stack.pop() << "\n";
    std::cout << *stack.pop() << "\n";
    std::cout << *stack.pop() << "\n";
    //std::cout << *stack.pop() << "\n";
    stack.push(new int(3));
    std::cout << *stack.pop() << "\n";
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
